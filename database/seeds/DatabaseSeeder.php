<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        /*
              $table->string('names')->nullable();
            $table->string('surnames')->nullable();
            $table->string('address')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('phones')->nullable();
            $table->string('dni')->nullable();
        */
        App\Models\User::create([
            'id' =>'1', 
            'names' => 'Adminstrador',
            'surnames' => '',
            'address' => '',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'phones'=> '',
            'dni'=> '',
            
        ]);

    }
}
