<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping', function (Blueprint $table) { //tabla de envios
            $table->increments('id');
            $table->unsignedInteger('box_id'); //relacion tabla cajas
            $table->unsignedInteger('orders_id'); //relacion tabla pedidos
            $table->date('date');
            $table->timestamps();
            $table->foreign('box_id')->references('id')->on('boxes');
            $table->foreign('orders_id')->references('id')->on('orders');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping');
    }
}
