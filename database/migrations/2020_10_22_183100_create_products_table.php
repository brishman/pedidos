<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->string('product_code')->nullable();
            $table->string('size')->nullable();
            $table->string('gender')->nullable();
            $table->string('colour')->nullable();
            $table->unsignedInteger('brands_id'); //relacion tabla marcas
            $table->unsignedInteger('categories_id'); //relacion tabla categorias
            $table->timestamps();
            $table->foreign('brands_id')->references('id')->on('brands');
            $table->foreign('categories_id')->references('id')->on('categories');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
