<?php
  Route::post('/authenticate', 'UserController@loginapi');
  Route::get('notifications/student/{facultad}/{seme_f_incodaca}','M_noti_persController@notifications');
  Route::get('logout', 'AuthController@logout');
  Route::get('faculties/records','M_facuController@records');
  Route::get('levels/records','S_niveController@records');
  Route::get('typeusers/records','S_tipo_usuaController@records');
  Route::get('semesters/records','S_seme_acadController@records');
  Route::get('notifications/students/readnotifications/{tipo}/{id}','M_noti_persController@contarstudentnotificaciones');

  Route::group(['middleware' => ['jwt.auth']], function() {
  Route::get('teachers/messages/{faculties}','MessagesController@messages');
  Route::get('register', 'UserController@index')->name('register.index');
  Route::get('users/perfil','UserController@perfil')->name('perfil.index');
  Route::get('users/load_perfil','UserController@loadPerfil');
  Route::get('users/columns','UserController@columns');
  Route::get('users/records','UserController@records');
  Route::get('users/tables','UserController@tables');
  Route::get('users/record/{item}','UserController@record');
  Route::post('users/save_perfil','UserController@updateperfil');
  Route::post('users', 'UserController@store');
  Route::delete('users/baja/{item}','UserController@destroy');
  Route::get('users/assigned/{item}','UserController@assigned');
  Route::get('notifications/teachers/{facultad}','M_noti_persController@notifications_teachers');
  Route::get('notifications/teachers/readnotification/{tipo}/{id}','M_noti_persController@contarnotificaciones');    
   });

 //});
