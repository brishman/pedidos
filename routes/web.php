<?php
    Auth::routes();
    Route::get('login', 'UserController@showLogin')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::group(['middleware' => ['auth']], function() {
        Route::get('/dashboard', function () {
            return view('home');
        });        
        Route::get('/', function () {
       //         return view('home');
       return redirect('dashboard');
       return redirect()->route('dashboard');
    });
    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    Route::get('register', 'UserController@index')->name('register.index');
    Route::get('user/perfil','UserController@perfil')->name('perfil.index');
    Route::get('user/load_perfil','UserController@loadPerfil');
    Route::get('user/columns','UserController@columns');
    Route::get('user/records','UserController@records');
    Route::get('user/tables','UserController@tables');
    Route::get('user/record/{item}','UserController@record');
    Route::post('user/save_perfil','UserController@updateperfil');
    Route::post('user', 'UserController@store');
    Route::delete('user/baja/{item}','UserController@destroy');
    Route::get('user/assigned/{item}','UserController@assigned');
    
    //customers---------------------------------------------------------clientes
 Route::get('customer','CustomerController@index')->name('customer.index');
 Route::get('customer/columns','CustomerController@columns');
 Route::get('customer/records','CustomerController@records');
 Route::get('customer/record/{item}','CustomerController@record');
 Route::post('customer', 'CustomerController@store');
 Route::delete('customer/baja/{item}','CustomerController@destroy');

 //boxes---------------------------------------------------------cajas
 Route::get('box','BoxController@index')->name('box.index');
 Route::get('box/columns','BoxController@columns');
 Route::get('box/records','BoxController@records');
 Route::get('box/record/{item}','BoxController@record');
 Route::post('box','BoxController@store');
 Route::delete('box/baja/{item}','BoxController@destroy');

 //brands---------------------------------------------------------marcas
 Route::get('brand','BrandController@index')->name('brand.index');
 Route::get('brand/columns','BrandController@columns');
 Route::get('brand/records','BrandController@records');
 Route::get('brand/record/{item}','BrandController@record');
 Route::post('brand','BrandController@store');
 Route::delete('brand/baja/{item}','BrandController@destroy');

 //categories---------------------------------------------------------categorias
 Route::get('category','CategoryController@index')->name('category.index');
 Route::get('category/columns','CategoryController@columns');
 Route::get('category/records','CategoryController@records');
 Route::get('category/record/{item}','CategoryController@record');
 Route::post('category','CategoryController@store');
 Route::delete('category/baja/{item}','CategoryController@destroy');

 //products---------------------------------------------------------productos
 Route::get('product','ProductController@index')->name('product.index');
 Route::get('product/columns','ProductController@columns');
 Route::get('product/tables','ProductController@tables');
 Route::get('product/records','ProductController@records');
 Route::get('product/record/{item}','ProductController@record');
 Route::post('product','ProductController@store');
 Route::delete('product/baja/{item}','ProductController@destroy');
 //orders---------------------------------------------------------pedidos
 Route::get('orders','OrderController@index')->name('order.index');
 Route::get('orders/columns','OrderController@columns');
 Route::get('orders/tables','OrderController@tables');
 Route::get('orders/search/products', 'OrderController@searchProducts');
 Route::get('orders/search/customers', 'OrderController@searchCustomers');

 Route::get('orders/records','OrderController@records');
 Route::get('orders/record/{item}','OrderController@record');
 Route::post('orders','OrderController@store');
 Route::delete('orders/{item}','OrderController@destroy');
 Route::delete('orders/detail/{id}','OrderController@destroy_detail');
 //shipping---------------------------------------------------------envios
 Route::get('shipping','ShippingController@index')->name('shipping.index');
 Route::get('shipping/columns','ShippingController@columns');
 Route::get('shipping/records','ShippingController@records');
 Route::get('shipping/tables','ShippingController@tables');
 Route::get('shipping/record/{item}','ShippingController@record');
 Route::post('shipping','ShippingController@store');
 Route::get('shipping/pedidos','ShippingController@pedidos');
 Route::delete('shipping/detail/{id}','ShippingController@destroy_detail');

 Route::delete('shipping/baja/{item}','OrderController@destroy');









 
    });
 