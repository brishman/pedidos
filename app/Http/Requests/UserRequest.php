<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'names'                         => ['required'],
            'surnames'                      => ['required'],
            'address'                       => ['required'],
            'email'                         => ['required'],
            'phones'                        => ['required'],
            'dni'                           => ['required'],
            'password'                      => ['required'],
            

          ];
      }
    }
    public function messages()
    {
        return [
            'names.required'                        => 'Es obligatorio.',
            'surnames.required'                     => 'Es obligatorio.',
            'address.required'                      => 'Es obligatorio.',
            'email.required'                        => 'Es obligatorio.',
            'phones.required'                       => 'Es obligatorio.',
            'dni.required'                          => 'Es obligatorio.',
            'password.required'                     => 'Es obligatorio.',







            

        ];
    }
}
