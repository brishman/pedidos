<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BrandRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'brand'                  =>  ['required'],
            
          ];
      }
    }
    public function messages()
    {
        return [
            'brand.required'             => 'Es obligatorio.',
                       
        ];
    }
}
