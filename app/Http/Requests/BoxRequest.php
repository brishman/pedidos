<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BoxRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'description'                  =>  ['required'],
            
          ];
      }
    }
    public function messages()
    {
        return [
            'description.required'             => 'Es obligatorio.',
                       
        ];
    }
}
