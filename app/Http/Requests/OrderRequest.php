<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'date'                          =>  ['required'],
            'customer_id'                  =>  ['required'],
           
          ];
      }
    }
    public function messages()
    {
        return [
            'date.required'                     => 'Es obligatorio.',
            'quantity.required'                 => 'Es obligatorio.',
            'products_id.required'              => 'Es obligatorio.',
            'customers_id.required'             => 'Es obligatorio.',
            
        ];
    }
}
