<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ShippingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'date'                          =>  ['required'],
            'quantity'                      =>  ['required'],
            'box_id'                      =>  ['required'],     
            'orders_id'                     =>  ['required'],
           
          ];
      }
    }
    public function messages()
    {
        return [
            'date.required'                     => 'Es obligatorio.',
            'quantity.required'                 => 'Es obligatorio.',
            'boxes_id.required'                 => 'Es obligatorio.',
            'products_id.required'              => 'Es obligatorio.',
            'orders_id.required'                => 'Es obligatorio.',
            
        ];
    }
}
