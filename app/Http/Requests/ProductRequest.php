<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){
      {
          $id = $this->input('id');
          return [
            'description'                  =>  ['required'],
            'product_code'                 =>  ['required'],
            'size'                         =>  ['required'],     
            'gender'                       =>  ['required'],
            'colour'                       =>  ['required'],
            'brands_id'                    =>  ['required'],
            'categories_id'                =>  ['required'],
          ];
      }
    }
    public function messages()
    {
        return [
            'description.required'             => 'Es obligatorio.',
            'product_code.required'            => 'Es obligatorio.',
            'size.required'                    => 'Es obligatorio.',
            'gender.required'                  => 'Es obligatorio.',
            'colour.required'                  => 'Es obligatorio.',
            'brands_id.required'               => 'Es obligatorio.',
            'categories_id.required'           => 'Es obligatorio.',
            
        ];
    }
}
