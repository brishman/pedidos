<?php
namespace App\Http\Controllers;
use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\Customer;
use App\Models\Product;
use App\Models\DetailOrders;
use Exception;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()                     //Llama al template Blade de laravel
    {
        return view('orders.index');
        
    }
    public function searchCustomers(Request $request){
        $customers = Customer::where('dni','like', "%{$request->input}%")
                            ->orWhere('names','like', "%{$request->input}%")
                            ->get()->transform(function($row) {
                                return [
                                    'id' => $row->id,
                                    'description' => $row->dni.' - '.$row->names.' - '.$row->surnames,
                                    'name' =>$row->names.' - '.$row->surnames,
                                    'number' => $row->dni
                                ];
                            }); 
         return compact('customers');
    }
    public function searchProducts(Request $request){
        $products = Product::where('description','like', "%{$request->input}%")
                            ->get()->transform(function($row) {
                                return [
                                    'id' => $row->id,
                                    'description' => $row->description,
                                    'stock' => $row->stock,
                                ];
                            }); 
         return compact('products');
                        }
    public function columns()//buscador x campo
    {
        return [
            'id'            => 'Código',
            'date'          => 'Fecha',
            
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = Order::where($request->column, 'like', "%{$request->value}%")->orderBy($request->column);//para ordenar
         return new OrderCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new OrderResource(Order::findOrFail($id));
        return $record;
    }

    public function store(OrderRequest $request){//Guardar y Actualizar
        $id    = $request->input('id');
        $order = Order::firstOrNew(['id' => $id]);
        $order->fill($request->all());
        $order->date=Carbon::parse($request->input('date'))->format('Y-m-d');
        $order->save();
   //     dd($request->items);
        foreach($request->items as $detail) {
            DetailOrders::updateOrCreate(['id' =>$detail['id']], [
            'order_id'    =>$order->id,
            'quantity'     =>$detail['quantity'],
            'product_id'  =>$detail['product_id'],
           
         ]); 
        }
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$order
        ];
    }
    public function destroy($id)//Eliminar
    {
        $order = Order::findOrFail($id);
        $order->delete();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
    public function destroy_detail($id){
        $order = DetailOrders::findOrFail($id);
        $order->delete();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
    
}
