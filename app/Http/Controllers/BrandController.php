<?php
namespace App\Http\Controllers;
use App\Http\Requests\BrandRequest;
use App\Http\Resources\BrandCollection;
use App\Http\Resources\BrandResource;
use App\Models\Brand;
use Exception;
use App\Http\Controllers\Controller;
//use Carbon\Carbon;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()                     //Llama al template Blade de laravel
    {
        return view('brand.index');
        
    }
    public function columns()//buscador x campo
    {
        return [
            'id'            => 'Código',
            'brand'         => 'Marca',
            //'name2' => 'Buscador2'
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = Brand::where($request->column, 'like', "%{$request->value}%")->orderBy($request->column);//para ordenar

        return new BrandCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new BrandResource(Brand::findOrFail($id));
        return $record;
    }

    public function store(BrandRequest $request){//Guardar y Actualizar
        $id       = $request->input('id');
        $brand    = Brand::firstOrNew(['id' => $id]);
        $brand->fill($request->all());
        //$personal->date_nac=Carbon::parse($request->input('date_nac'))->format('Y-m-d');
        $brand->save();
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$brand
        ];
    }

    public function destroy($id)//Eliminar
    {
        $brand = Brand::findOrFail($id);
        $brand->delete();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
 
}
