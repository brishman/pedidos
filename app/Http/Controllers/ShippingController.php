<?php
namespace App\Http\Controllers;
use App\Http\Requests\ShippingRequest;
use App\Http\Resources\ShippingCollection;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\ShippingResource;
use App\Models\Shipping;
use App\Models\Box;
use App\Models\Product;
use App\Models\Order;
use App\Models\DetaiShipping;
use Exception;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use Illuminate\Http\Request;

class ShippingController extends Controller
{
    public function index()                     //Llama al template Blade de laravel
    {
        return view('shipping.index');
        
    }
    public function pedidos(Request $request){
       
        $records = Order::where($request->column, 'like', "%{$request->value}%")->orderBy($request->column);//para ordenar
        return new OrderCollection($records->paginate(config('tenant.items_per_page')));
    }
    public function tables(){
       
        $boxes = Box::all(); 
        return compact('boxes');
     }
    public function columns()//buscador x campo
    {
        return [
            'id'            => 'Código',
            'date'          => 'Fecha',
            
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = Shipping::where($request->column, 'like', "%{$request->value}%")->orderBy($request->column);//para ordenar

        return new ShippingCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new ShippingResource(Shipping::findOrFail($id));
        return $record;
    }

    public function store(ShippingRequest $request){//Guardar y Actualizar
     //   dd($request->items);
        $id= $request->input('id');
        $shipping  = Shipping::firstOrNew(['id' => $id]);
        $shipping->fill($request->all());
        $shipping->date=Carbon::parse($request->input('date'))->format('Y-m-d');
        $shipping->save();
        foreach($request->items as $detail) {
            DetaiShipping::updateOrCreate(['id' =>$detail['id']], [
            'shipping_id'  =>$shipping->id,
            'quantity'     =>$detail['quantity'],
            'product_id'  =>$detail['product_id'],
           
         ]); 
        }
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$shipping
        ];
    }

    public function destroy($id)//Eliminar
    {
        $shipping = Shipping::findOrFail($id);
        $shipping->state='0';
        $shipping->save();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
    public function destroy_detail($id){
        $order = DetaiShipping::findOrFail($id);
        $order->delete();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }
}
