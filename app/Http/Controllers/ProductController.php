<?php
namespace App\Http\Controllers;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\Category;
use App\Models\Brand;
use Exception;
use App\Http\Controllers\Controller;
//use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()                     //Llama al template Blade de laravel
    {
        return view('product.index');
        
    }

    public function columns()//buscador x campo
    {
        return [
            'id'            => 'Código',
            'product_code'  => 'Codigo Producto',
            'gender'        => 'Genero'
        ];
    }
 
    public function tables()
    {
        $brand = Brand::all();
        $category = Category::all();
        return compact('brand','category');
    }

    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = Product::where($request->column, 'like', "%{$request->value}%")->orderBy($request->column);//para ordenar

        return new ProductCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new ProductResource(Product::findOrFail($id));
        return $record;
    }

    public function store(ProductRequest $request){//Guardar y Actualizar
        $id       = $request->input('id');
        $product  = Product::firstOrNew(['id' => $id]);
        $product->fill($request->all());
        //$personal->date_nac=Carbon::parse($request->input('date_nac'))->format('Y-m-d');
        $product->save();
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$product
        ];
    }

    public function destroy($id)//Eliminar
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }

}
