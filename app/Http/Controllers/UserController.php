<?php
namespace App\Http\Controllers;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Exception;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
class UserController extends Controller
{
    public function showLogin()             // para users 
    {
        return view('auth.login');
    }
    public function index()
    {
        return view('notificaciones.users.index');  
    }
    public function perfil()
    {
        return view('notificaciones.users.perfil');  
    }
    public function loadPerfil(){
        $user = User::findOrFail(auth()->user()->id);
        return $user;
    }
    public function updateperfil(UpdateUserRequest $request){
        $id = $request->input('id');
        $user =User::firstOrNew(['id' => $id]);
        $user->fill($request->all());
        $user->password= bcrypt($request->input('new_password'));
        $user->save();
     //   Auth::logout();
       return [
        'success' => true,
        'message' => 'Actualizado perfil con éxito',
        'data'     =>$user
    ];
    }
    public function columns()//buscador
    {
        return [
            'name' => 'Nombre',
            'email' => 'E-mail',
        ];
    }
    /*public function tables(){
        $tipousuario=S_tipo_usua::all();
        $niveles=S_nive::all();
        $facultad=M_facu::all();
        $semestre=S_seme_acad::all();
        $nivelsession=User::findOrFail(Auth::user()->id);
        $useractive=S_nive::where('id',Auth::user()->nive_f_incodniv)->first();
        $facultadActive=M_facu::where('id',Auth::user()->facu_f_incodfac)->first();
        return compact('tipousuario','niveles','facultad','semestre','nivelsession','useractive','facultadActive');
    }*/
    public function records(Request $request)  //para ordenar
    {
        $records = User::where($request->column, 'like', "%{$request->value}%")
                    ->orderBy('id');

        return new UserCollection($records->paginate(config('items_per_page')));
    }
    public function record($id)
    {
        $record = new UserResource(User::findOrFail($id));// modificar cambiar customer
        return $record;
    }
  /* public function assigned($id){
        $assigned = Assigned::where('user_id', '=', $id)->get();
        return $assigned;
    }*/
   /* public function impotarexcel(Request $request){
    try {
        (new UserImport)->import(request()->file('file')); 
        return response()->json([
            'success' => true,
            'message' => 'Se importo con Exito',
        ]);

    } catch (Exception $e) {
             return response()->json(['success'=>true,'msg' =>$e]);             
    }           
       
      
    }*/
    public function store(UserRequest $request){
        DB::beginTransaction();
        try {  
        $id = $request->input('id');
        $user =User::firstOrNew(['id' => $id]);
        $user->fill($request->all());
        //if($request->input('password')!=null){
        $user->password= bcrypt($request->input('new_password'));
        //}
        $user->save();
        DB::commit();

        return [
            'success' => true,
            'message' => ($id)?'Modificado con éxito':'Registrado con éxito',
            'data'     =>$user
        ];
    } catch (Exception $e) {
        DB::rollBack();
        return $this->errorResponse($e, 409);
    }
    }
    public function destroy($id){
      //  Assigned::where('user_id', $id)->delete();
        $user = User::findOrFail($id);// cambiar
        $user->delete();
        return [
            'success' => true,
            'message' => 'Usuario eliminado con éxito'
        ];
    }
    public function loginapi(Request $request)
    {
        $credentials = $request->only('email', 'password');
        
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 401);
        }
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'success' => false, 
                    'error' => 'No coincide los Credenciales'], 404);
            }
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'error' => 'Failed to login, please try again.'], 500);
        }
        $perfil=User::where('email',$request->input('email'))->first();
        return response()->json([
            'success' => true, 
            'perfil'  =>$perfil,  
            'data'=> [ 'token' => $token ]
        ], 200);
    }
}
