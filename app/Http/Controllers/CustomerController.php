<?php
namespace App\Http\Controllers;
use App\Http\Requests\CustomerRequest;
use App\Http\Resources\CustomerCollection;
use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use Exception;
use App\Http\Controllers\Controller;
//use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()                     //Llama al template Blade de laravel
    {
        return view('customer.index');
        
    }

    public function columns()//buscador x campo
    {
        return [
            'id' => 'Código',
            'dni' => 'N° Dni',
            //'name2' => 'Buscador2'
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = Customer::where($request->column, 'like', "%{$request->value}%")->orderBy($request->column);//para ordenar

        return new CustomerCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new CustomerResource(Customer::findOrFail($id));
        return $record;
    }

    public function store(CustomerRequest $request){//Guardar y Actualizar
        $id       = $request->input('id');
        $customer = Customer::firstOrNew(['id' => $id]);
        $customer->fill($request->all());
        //$personal->date_nac=Carbon::parse($request->input('date_nac'))->format('Y-m-d');
        $customer->save();
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$customer
        ];
    }

    public function destroy($id)//Eliminar
    {
        $customer = Customer::findOrFail($id);
        $customer->state='0';
        $customer->save();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }

}
