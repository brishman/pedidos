<?php
namespace App\Http\Controllers;
use App\Http\Requests\BoxRequest;
use App\Http\Resources\BoxCollection;
use App\Http\Resources\BoxResource;
use App\Models\Box;
use Exception;
use App\Http\Controllers\Controller;
//use Carbon\Carbon;
use Illuminate\Http\Request;

class BoxController extends Controller
{
    public function index()                     //Llama al template Blade de laravel
    {
        return view('box.index');
        
    }

    public function columns()//buscador x campo
    {
        return [
            'id'            => 'Código',
            'description'   => 'Descripción',
            //'name2' => 'Buscador2'
        ];
    }
 
    public function records(Request $request)//Genera lista de registro para el grid
    {
        $records = Box::where($request->column, 'like', "%{$request->value}%")->orderBy($request->column);//para ordenar

        return new BoxCollection($records->paginate(config('tenant.items_per_page')));
    }

   
    public function record($id)//Selecccionar un Registro
    {
        $record = new BoxResource(Box::findOrFail($id));
        return $record;
    }

    public function store(BoxRequest $request){//Guardar y Actualizar
        $id       = $request->input('id');
        $box      = Box::firstOrNew(['id' => $id]);
        $box->fill($request->all());
        //$personal->date_nac=Carbon::parse($request->input('date_nac'))->format('Y-m-d');
        $box->save();
        return [
            'success' => true,
            'message' => ($id)?'Actualizado con éxito':'Registrado con éxito',
            'data'    =>$box
        ];
    }

    public function destroy($id)//Eliminar
    {
        $box = Box::findOrFail($id);
        $box->delete();
        return [
            'success' => true,
            'message' => 'Eliminado con éxito'
        ];
    }


}
