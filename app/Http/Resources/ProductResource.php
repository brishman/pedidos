<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'description'         => $this->description,
            'product_code'        => $this->product_code,
            'size'                => $this->size,
            'gender'              => $this->gender,
            'colour'              => $this->colour,
            'brands_id'           => $this->brands_id,
            'categories_id'       => $this->categories_id,
        ];
    }
}
