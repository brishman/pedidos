<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
class ShippingCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id'                => $row->id,
                'date'              => $row->date,
                'orders'            => $row->orders,
                'shipping_details'  => $row->shipping_details,
                'boxes'             => $row->boxes,
                       
            ];
        });
    }
}
