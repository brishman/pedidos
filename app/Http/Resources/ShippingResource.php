<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class ShippingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'date'              => $this->date,
            'orders'            => $this->orders,
            'items'             => $this->shipping_details,
            'boxes'             => $this->boxes,
           
        ];
    }
}
