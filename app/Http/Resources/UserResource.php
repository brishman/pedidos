<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'names'         => $this->names,
            'surnames'      => $this->surnames,
            'address'       => $this->address,
            'email'         => $this->email,
            'phones'        => $this->phones,
            'dni'           => $this->dni,
            'password'      => $this->password,
            
        ];
    }
}
