<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id'            => $row->id,
                'names'         => $row->names,
                'surnames'      => $row->surnames,
                'address'       => $row->address,
                'email'         => $row->email,
                'phones'        => $row->phones,
                'dni'           => $row->dni,
                'password'      => $row->password,          
            ];
        });
    }
}
