<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;
class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id'                 => $row->id,
                'description'        => $row->description,
                'product_code'       => $row->product_code,
                'size'               => $row->size,
                'gender'            => $row->gender,
                'colour'            => $row->colour,
                'brands_id'         => $row->brands_id,
                'categories_id'     => $row->categories_id,
                      
            ];
        });
    }
}
