<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table="customers";         //nombre tabla
    protected $primarykey="id";
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        'names',              //nombres
        'surnames',           // apellidos        
        'address',            //direccion       
        'email',              //correo         
        'phones',             //telefonos     
        'dni',                //dni

    ];
}
