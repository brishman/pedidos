<?php

namespace App\Models;
use transformers\UserTransformer;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable 
{
    use Notifiable;
    //protected $with =['tipo_usuario','nivel_usuario','facultad']; // relacion de tablas 

    protected $table="users";
    protected $primaryKey="id";
    protected $fillable = [
        'names',              //nombres
        'surnames',           //apellidos         
        'address',            //direccion          
        'email',              //correo     
        'phones',             //telefonos        
        'dni',                //numero dni
        'password',           //contrasena 
        
        
    ];

    
    protected $hidden = ["created_at", "updated_at","password","remember_token"];
  
   
   
}
