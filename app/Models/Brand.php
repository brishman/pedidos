<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table="brands";         //nombre tabla
    protected $primarykey="id";
    protected $hidden = ["created_at", "updated_at"];
    protected $fillable = [
        'brand',
    ];
    
}
