<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $table="boxes";         //nombre tabla
    protected $primarykey="id";
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        'description',              //descripcion
        'quantity'
    ];
    
}
