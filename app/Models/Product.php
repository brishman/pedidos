<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $with = ['brands','categories'];
    protected $table="products";         //nombre tabla
    protected $primarykey="id";
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        'description',              //descripcion
        'product_code',             //codigoproducto        
        'size',                     //talla       
        'gender',                   //genero        
        'colour',                   //color     
        'brands_id',                //marca
        'categories_id',           //categoria
        ];

        public function brands()
    {
        return $this->belongsTo(Brand::class,'brands_id','id');
    }  

    public function categories()
    {
        return $this->belongsTo(CategorY::class,'categories_id','id');
    }  
}
