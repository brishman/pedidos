<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $with = ['clientes','items'];
    protected $table="orders";         //nombre tabla
    protected $primarykey="id";
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        'date',                         //fecha
         'customer_id',                 //clientes
         'state'
        ];

    public function clientes()
    {
        return $this->belongsTo(Customer::class,'customer_id','id');
    }  
 
    public function items()
    {
        return $this->hasMany('App\Models\DetailOrders', 'order_id');
     } 
}
