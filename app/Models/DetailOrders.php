<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class DetailOrders extends Model
{
    protected $with = ['product'];
    protected $table="detailorders";         //nombre tabla
    protected $primarykey="id";
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        'order_id',            
        'quantity',                       
        'product_id',                   
         ];

       
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    } 
    
}
