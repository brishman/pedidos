<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $with = ['boxes','orders','shipping_details'];
    protected $table="shipping";         //nombre tabla
    protected $primarykey="id";
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        'box_id',                             
        'orders_id',                           
        'date'                
        ];

        public function boxes()
    {
        return $this->belongsTo(Box::class,'box_id','id');
    }  
    public function orders()
    {
        return $this->belongsTo(Order::class,'orders_id','id');
    }  
    public function shipping_details()
    {
        return $this->hasMany('App\Models\DetaiShipping', 'shipping_id');
     } 
}
