<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table="roles";
    protected $primaryKey="id";
    protected $fillable = [
        'name', 'display_name'
    ];
    protected $hidden = ["created_at", "updated_at"];

    public function user()
    {
    	return $this->hasMany(User::class);
    }
}