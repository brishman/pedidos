<?php

namespace App\Imports;

use App\Models\User;
use App\Models\M_facu;
use App\Models\S_tipo_usua;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\DB;
class UserImport implements ToModel,WithHeadingRow,WithValidation
{
    use Importable;
    public function model(array $row)
    {
        $userActivo=User::findOrFail(Auth::user()->id);
            $facultad=M_facu::where('facu_chnomfac','like',$row['facu_f_incodfac'])->first();
            $tipoUser=S_tipo_usua::where('tipo_chnomusu','like',$row['tipo_f_incodusu'])->first();
        return new User([
            'tipo_f_incodusu' => $tipoUser->id,
            'nive_f_incodniv' =>  $userActivo->nive_f_incodniv,
            'facu_f_incodfac' => $facultad->id,
            'name'            => $row['name'],
            'pers_chdocide'   => $row['pers_chdocide'],
            'pers_chcelper'   => $row['pers_chcelper'],
            'pers_chdirper'   => $row['pers_chdirper'],
            'email'           => $row['email'],
            'password'        => bcrypt($row['password']),
            'token'           => '',
            'usua_boestusu'   =>true,
            'new_password'    => $row['password'],
        ]);
    }
    public function rules(): array
    {
        return [
            '*.email' =>'required|email|unique:users',
            '*.pers_chdocide' =>'required|unique:users'
            ,
        ];
    }
    public function customValidationMessages()
{
    return [
        '*.email' => 'Correo ya esta en uso.',
    ];
}

}
