import moment from 'moment';
export const deletable = {
    methods: {
        async generar_correrecibo(){
            const response= await this.$http.get(`/contratos/correrecibo`);
            this.form.payment_number=response.data;
       },
       acuentas(valor){
            if(valor=="1"){
                let calcular=parseFloat(this.form.total)-parseFloat(this.form.on_account);
                this.form.balance=calcular.toFixed(2);
            }else{
                let calcular=parseFloat(this.form_pagos.balance2)-parseFloat(this.form_pagos.onaccount_payment);
                this.form_pagos.balance_payment=calcular.toFixed(2);
            }
            if(this.form.on_account>0 && valor=="1"){
                this.form.generar_recibo="Si"
            }else{
                this.form.generar_recibo="No"
            }
             if(this.form.payment_number!=null && this.form.generar_recibo=="Si"){
                 console.log("Generar un Recibo")
           if(!this.recordId){
                 let array=_.filter(this.array_recibos,{'nuevo': 'No'});
                 this.generar_newrecibo(0,0,"Adelanto de Pago por Servicio de Funeraria",moment().format('YYYY-MM-DD'),this.form.total,this.form.on_account,this.form.balance, this.form.payment_number,'Si',array)
           }
           }
    },
       generar_newrecibo(id,proforma_id,description,date_payment,total,onaccount_payment,balance_payment,payment_number,nuevo,array){
         
          this.form.dataRecibo.push({
              id:id,
              proforma_id:proforma_id,
              description:description,
              date_payment:date_payment,
              total:total,
              onaccount_payment:onaccount_payment,
              balance_payment:balance_payment,
              payment_number:payment_number,
              nuevo:nuevo  
          })
      },
        buscar_dni(dni,num) {
            return new Promise((resolve) => {
                    this.$http.get(`consultar/buscardni/${dni}`)
                        .then(res => {
                            if(res.data.success) {
                            if(num=="1"){
                                this.form.requested=res.data.data.nombres+" "+res.data.data.apellidoPaterno+" "+res.data.data.apellidoMaterno
                                resolve()
                            }else if(num=="2"){
                                this.form.dead=res.data.data.nombres+" "+res.data.data.apellidoPaterno+" "+res.data.data.apellidoMaterno
                                resolve()
                            }else if(num=="3"){
                                this.form.acreditado=res.data.data.nombres+" "+res.data.data.apellidoPaterno+" "+res.data.data.apellidoMaterno
                                resolve()
                            }
                            }else{
                                this.$message.error(res.data.message);
                            }
                        })
                      
                })
            
        },
        delete_destroy(url){
            return new Promise((resolve) => {
                this.$http.delete(url)
                .then(res => {
                    if(res.data.success) {
                        this.$message.success('Se eliminó correctamente el registro')
                        resolve()
                    }
                })
            })
        },
        destroy(url) {
            return new Promise((resolve) => {
                this.$confirm('Desea dar de baja el registro Seleccionado?', 'Eliminar', {
                    confirmButtonText: 'Eliminar',
                    cancelButtonText: 'Cancelar',
                    type: 'warning'
                }).then(() => {
                    this.$http.delete(url)
                        .then(res => {
                            if(res.data.success) {
                                this.$message.success('Se eliminó correctamente el registro')
                                resolve()
                            }
                        })
                        .catch(error => {
                            if (error.response.status === 500) {
                                this.$message.error(res.data.message);
                            } else {
                                console.log(error.response.data.message)
                            }
                        })
                }).catch(error => {
                    console.log(error)
                });
            })
        },
        alta(url) {
            return new Promise((resolve) => {
                this.$confirm('¿Desea dar de alta el registro Seleccionado?', 'Aviso', {
                    confirmButtonText: 'Eliminar',
                    cancelButtonText: 'Cancelar',
                    type: 'warning'
                }).then(() => {
                    this.$http.delete(url)
                        .then(res => {
                            if(res.data.success) {
                                this.$message.success(res.data.message)
                                resolve()
                            }
                        })
                        .catch(error => {
                            if (error.response.status === 500) {
                                this.$message.error('Error al intentar dar alta');
                            } else {
                                console.log(error.response.data.message)
                            }
                        })
                }).catch(error => {
                    console.log(error)
                });
            })
        },
    }
}
