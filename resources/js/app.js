require('./bootstrap');
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Axios from 'axios'
import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'

locale.use(lang)
//Vue.use(ElementUI)
Vue.use(ElementUI, {size: 'small'})
Vue.prototype.$eventHub = new Vue()
Vue.prototype.$http = Axios

Vue.component('x-graph', require('./components/graph/src/Graph.vue').default);
Vue.component('x-graph-line', require('./components/graph/src/GraphLine.vue').default);
Vue.component('dashboard', require('./views/dashboard/index.vue').default);
////////////////////////////////////////////////////////////////////////////////////////////////////////
Vue.component('customers-form', require('./views/customer/index.vue').default);
Vue.component('boxes-form', require('./views/box/index.vue').default);
Vue.component('brand-form', require('./views/brand/index.vue').default);
Vue.component('category-form', require('./views/category/index.vue').default);
Vue.component('product-form', require('./views/product/index.vue').default);
Vue.component('order-forms', require('./views/orders/index.vue').default);
Vue.component('shipping-index', require('./views/shipping/index.vue').default);
Vue.component('x-form-group', require('./components/FormGroup.vue').default);

const app = new Vue({
    el: '#default',
    /*
    mounted:function(){
        Echo.private('notifications')
            .listen('UserSessionChanged', (e) => {
            const notificationElement = document.getElementById('notification');
            const h = this.$createElement;
            this.$message({
                showClose: true,
                message:e.message,
                type: e.type
              });
 
});
  
    }
    */
});
