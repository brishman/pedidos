<aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile" style="background: url({{ asset('assets/images/background/user-info.jpg') }}) no-repeat;">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="{{ asset('assets/images/users/profile.png') }}" alt="user" /> </div>
                    <!-- User profile text-->
                       <!-- User profile text-->
                       <div class="profile-text"> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">{{ Auth::user()->name }}</a>
                        <div class="dropdown-menu animated flipInY">
                            <a href="{{ url('user/perfil') }}" class="dropdown-item"><i class="ti-user"></i> Mi Perfil</a>
                            <div class="dropdown-divider"></div> 
                            <a href="{{route('logout')}}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="feather icon-power"></i> Cerrar Sistema
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                 </a>
                             
                        </div>
                    </div>
                  
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-laptop-windows"></i><span
                                    class="hide-menu">Mantenimiento</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                                <li><a href="{{ url('/brand') }}">Registro Marca</a></li>
                                <li><a href="{{ url('/category') }}">Registro Clasificacion</a></li>
                                <li><a href="{{ url('/product') }}">Registro Productos</a></li>
                                <li><a href="{{ url('/box') }}">Registro Cajas</a></li>
                                <li><a href="{{ url('/orders') }}">Registro Ordenes</a></li>
                                <li><a href="{{ url('/shipping') }}">Registrar Envios</a></li>
                             </ul>
                        </li>
                        
                        
                        
                       
                      
                       
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            
        </aside>