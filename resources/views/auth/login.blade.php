@extends('layouts.auth')
@section('content')
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <section id="wrapper">
        <div class="login-register" style="background-image:url(../assets/images/background/login-register.jpg);">
            <div class="login-box card">
                <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                        <h3 class="box-title m-b-20"><i class="fa fa-user-circle-o"></i> Inicio de Sessión</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text " id="basic-addon1"><i class="fa fa-envelope" aria-hidden="true"></i> </span>
                                </div>
                                <input class="form-control" name="email" type="text" required="" placeholder="Email"> 
                            </div>
                               
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text " id="basic-addon1"><i class="fa fa-key" aria-hidden="true"></i> </span>
                                </div>
                                <input class="form-control" name="password" type="password" required="" placeholder="Clave"> 
                            </div>

                               
                            </div>
                        </div>

                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit"><i class="fa fa-lock" aria-hidden="true"></i> Ingresar</button>
                            </div>
                        </div>
                       
                        
                    </form>
                     
                </div>
            </div>
        </div>
    </section>
@endsection
