<html>
<head>
    <style>
        @page {
            margin: 0cm 0.5cm 0cm 0.5cm;
            font-family: Arial;
        }
 
        body {
            margin: 1.5cm 0.5cm 0.5cm 0.5cm;
            font-family:arial;
            font-size:12px;
        }
 
        header {
            position: fixed;
            height: 1cm;
            color: #000;
            text-align: center;
            padding:10px;
            font-size:12px;
            font-family:arial;
           
        }
        thead{
            background-color: #eee;
            font-weight:bold;
            font-family:Arial;
            font-size:12px;
        }

        td{
            height:40px;
        }
        .td{
            border-bottom:1px solid #ddd;
            border-left:1px solid #ddd;
            padding:5px;
            font-family:Arial;
            font-size:11px;
        }
        th{
            height:40px;    
        }
        .stockcero{
            background-color:#FFDCD4;
            color:#000;
        }
        footer {
            position: fixed;
            bottom: 10px;
            height: 0.8cm;
            color:#000;
            text-align: center;
            font-size:11px;
            padding:12px;
            font-family:Arial;
            padding:10px;
        }
        
    </style>
</head>
<body>
<header>
   <table width="100%" border="0" style="border-collapse: collapse;border-bottom:1px solid #ddd;">
        <tr>
            <td width="70%" height="30" align="left" valign="top">
                <img src="{{ asset('css/logo_universidad.jpg') }}"/>
            </td>
            <td width="30%" height="30" align="right" valign="middle"><b>Reporte de Notificaciones Enviadas</b></td>
        </tr>
   </table>
</header>
<main>
@if($notificaciones->isNotEmpty())
<table width="100%" align="right" border="0" style="border-collapse: collapse;border:1px solid #ddd;">
  
    <tr>
        <td height="25"></td>
        <td height="25"><b>Quien Envio</b></td>
        <td height="25" align="left">{{$notificaciones[0]->nivel_usuario->nive_chnomniv}}</td>
        <td  height="25"><b>Destinatario</b></td>
        <td  height="25"  align="left">{{$notificaciones[0]->tipo_usuario->tipo_chnomusu}}</td>
    </tr>
    <tr>
        <td height="25"></td>
        <td height="25"><b>Facultad</b></td>
        <td height="25"  align="left">{{$notificaciones[0]->facultad->facu_chnomfac}}</td>
        <td  height="25"><b>Semestre</b></td>
        <td  height="25"  align="left">{{$notificaciones[0]->semestre->seme_chnomaca}}</td>
    </tr>
<thead>
    <tr>
        <td align="center">N°</td>
        <td align="center"> Titulo</td>
        <td align="center">Contenido</td>
        <td align="center">Fecha</td>
        <td align="center">Hora</td>
    </tr>
</thead>
<?php
$total=0;
?>
@foreach($notificaciones  as $row)
<tr>
<td class="td" align="center">{{$row->id}}</td>
     <td class="td">{{$row->title}}</td>
    <td class="td">{{$row->body}}</td>
    <td class="td">{{ Carbon\Carbon::parse($row->date)->format('d-m-Y') }}</td>
    <td class="td">{{ Carbon\Carbon::parse($row->created_at)->format('h:m:s') }}</td>
    
</tr>
@endforeach
<table>
@else
<table width="100%" border="0">
        <tr>
            <td  valign="middle" align="center"><h1>No hay registros</h1></td>
        </tr>
   </table>
@endif
</main>
 
<footer>
<table width="100%" border="0" style="border-collapse: collapse;border-top:1px solid #409EFF;">
        <tr>
            <td align="left" valign="middle" colspan="2">Direccion:</td>
        </tr>
   </table>
</footer>
</body>
</html>